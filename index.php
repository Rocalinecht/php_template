<?php
//________________________________> va chercher twig
require_once 'vendor/autoload.php';
//________________________________> va chercher le template

// $loader = new \Twig\Loader\ArrayLoader([
//     'index' => 'Hello {{ name }}!',
// ]);

$loader = new Twig_Loader_Filesystem(__DIR__.'/template');
$twig = new Twig_Environment($loader, [
    'cache'=> false,// __DIR__."/tmp"
]);

$faker = Faker\Factory::create();

echo $twig->render('index.twig',[
    'compagny_Name' =>$faker->company,
    'product_Adjective' =>$faker->name,
    'product_Name' => $faker->name,
    'product_Material'=>$faker->year($max = '2019') ,
    'url'=>$faker->url,
    'user_Name'=>$faker->name,
    'job'=> $faker->jobTitle,
    'color'=>$faker->safeColorName,
    'price'=>$faker->randomDigit,
    'email'=>$faker->freeEmail,
    'phone'=>$faker->e164PhoneNumber,
    'profil-img'=>$faker->imageUrl($width = 150, $height = 150),
    'product_img'=> $faker->imageUrl($width = 270, $height = 150),

    

    

    ]); 

    